﻿namespace ExcelToXml
{
    /// <summary>
    /// Элемент графа
    /// </summary>
    internal class Item
    {
        /// <summary>
        /// Идентфикатор элемента
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Идентфикатор родительского элемента
        /// </summary>
        public string ParentId { get; set; }
        /// <summary>
        /// Текст
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Признак посещения элемента (используется при обработке)
        /// </summary>
        public bool Visited { get; set; }

        public override string ToString()
        {
            return string.Format("{0} -> {1}:{2}", ParentId, Id, Text);
        }
    }
}