﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ExcelToXml
{
    /// <summary>
    /// Запись элементов в XML-документ
    /// </summary>
    internal static class XmlWriter
    {
        /// <summary>
        /// Запись элементов в XML-документ
        /// </summary>
        /// <param name="stream">Поток XML-документа</param>
        /// <param name="itemsLinks">Корневые элементы деревьев</param>
        public static void Write(Stream stream, IEnumerable<ItemLink> itemsLinks)
        {
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            var serializer = new XmlSerializer(typeof(ItemsRoot));
            using (var writer = System.Xml.XmlWriter.Create(stream,
                new XmlWriterSettings { Encoding = Encoding.UTF8, Indent = true }))
            {
                serializer.Serialize(writer, new ItemsRoot { Items = itemsLinks.ToList() }, ns);
            }
        }
    }
}