﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace ExcelToXml
{
    /// <summary>
    /// Элемент дерева
    /// </summary>
    public class ItemLink
    {
        /// <summary>
        /// Идентфикатор элемента
        /// </summary>
        [XmlAttribute("id")]
        public string Id { get; set; }
        /// <summary>
        /// Текст
        /// </summary>
        [XmlAttribute("text")]
        public string Text { get; set; }
        /// <summary>
        /// Потоки элемента
        /// </summary>
        [XmlElement("item", Type = typeof(ItemLink))]
        public List<ItemLink> Children { get; set; }

        public override string ToString()
        {
            var childrenIds = "";
            if (Children != null)
                childrenIds = string.Join(",", Children.Select(c => c.Id.ToString()));
            return string.Format("{0}:{1} -> {2}", Id, Text, childrenIds);
        }
    }
}