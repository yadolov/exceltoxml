﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExcelToXml
{
    /// <summary>
    /// Построитель дерево-образных связей между элементами.
    /// </summary>
    internal static class ForestBuilder
    {
        /// <summary>
        /// Построить дерево-образные связи между элементами.
        /// </summary>
        /// <param name="items">Элементы</param>
        /// <returns>Корневые элементы деревьев</returns>
        public static IEnumerable<ItemLink> Build(IEnumerable<Item> items)
        {
            if (items == null)
                throw new ArgumentNullException("items");

            // Извлекаем корневые элементы из вершин
            var itemsByParents = items.GroupBy(i => i.ParentId).ToDictionary(g => g.Key, g => g.ToList());
            var roots = ExtractRoots(items);
            var rootLinks = roots.Select(r => ExtractTree(r, itemsByParents)).ToList();

            // Извлекаем корневые элементы из циклов графа
            items = items.Where(i => !i.Visited);
            var itemsByChildren = items.GroupBy(i => i.Id).ToDictionary(g => g.Key, g => g.ToList());
            while (items.Any())
            {
                var root = ExtractRootFromCycle(items, itemsByChildren);
                if (root == null)
                    break;
                var rootLink = ExtractTree(root, itemsByParents);
                rootLinks.Add(rootLink);
                items = items.Where(i => !i.Visited);
            }

            return rootLinks;
        }

        /// <summary>
        /// Извлечь корни деревьев из элементов
        /// </summary>
        /// <param name="items">Элементы</param>
        /// <returns>Корневые элементы деревьев</returns>
        private static IEnumerable<Item> ExtractRoots(IEnumerable<Item> items)
        {
            var itemIds = new HashSet<string>(items.Select(i => i.Id));
            return items.Where(i => string.IsNullOrEmpty(i.ParentId) || i.Id == i.ParentId || !itemIds.Contains(i.ParentId));
        }

        /// <summary>
        /// Извлечь элемент для корня дерева из петли графа
        /// </summary>
        /// <param name="items">Элементы</param>
        /// <param name="itemsByChildren">Отношение идентификатора элемента к его родителям</param>
        /// <returns>Элемент корня дерева</returns>
        private static Item ExtractRootFromCycle(IEnumerable<Item> items, Dictionary<string, List<Item>> itemsByChildren)
        {
            var visitedItems = new HashSet<Item>();
            var item = items.First();
            visitedItems.Add(item);

            List<Item> itemParents;
            while (itemsByChildren.TryGetValue(item.ParentId, out itemParents))
            {
                item = itemParents.First();
                if (visitedItems.Contains(item))
                    return item;
                visitedItems.Add(item);
            }
            return null;
        }

        /// <summary>
        /// Извлечь корень дерева из циклов графа
        /// </summary>
        /// <param name="rootItem">Элемент корня дерева</param>
        /// <param name="itemsByParents">Отношение идентификатора элемента к его потомкам</param>
        /// <returns>Корневой элемент дерева</returns>
        private static ItemLink ExtractTree(Item rootItem, Dictionary<string, List<Item>> itemsByParents)
        {
            var visitedItems = new HashSet<Item>();
            visitedItems.Add(rootItem);
            rootItem.Visited = true;

            var rootLink = new ItemLink
            {
                Id = rootItem.Id,
                Text = rootItem.Text,
                Children = new List<ItemLink>()
            };

            var itemQueue = new Queue<ItemLink>();
            itemQueue.Enqueue(rootLink);

            while (itemQueue.Count != 0)
            {
                var item = itemQueue.Dequeue();
                List<Item> itemChildren;
                if (!itemsByParents.TryGetValue(item.Id, out itemChildren))
                    continue;
                foreach (var child in itemChildren)
                {
                    if (visitedItems.Contains(child))
                        continue;
                    visitedItems.Add(child);
                    child.Visited = true;
                    var childLink = new ItemLink
                    {
                        Id = child.Id,
                        Text = child.Text,
                        Children = new List<ItemLink>()
                    };
                    item.Children.Add(childLink);
                    itemQueue.Enqueue(childLink);
                }
            }

            return rootLink;
        }
    }
}