﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ExcelToXml
{
    /// <summary>
    /// Чтение данных из документа Excel
    /// </summary>
    internal static class ExcelReader
    {
        /// <summary>
        /// Чтение данных из документа Excel
        /// </summary>
        /// <param name="stream">Поток документа</param>
        /// <returns>Данные</returns>
        public static IEnumerable<Item> Read(Stream stream)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            using (var document = SpreadsheetDocument.Open(stream, false))
            {
                var workbookPart = document.WorkbookPart;
                var sheets = workbookPart.Workbook.Descendants<Sheet>();
                var sheet = sheets.FirstOrDefault();
                if (sheet == null)
                    throw new ArgumentException("Invalid document format: sheet no found", "stream");
                var workSheet = ((WorksheetPart)workbookPart.GetPartById(sheet.Id)).Worksheet;
                var sheetData = workSheet.Descendants<SheetData>().FirstOrDefault();
                if (sheetData == null)
                    throw new ArgumentException("Invalid document format: sheet data no found", "stream");
                var rows = sheetData.Descendants<Row>();
                var items = new List<Item>();
                foreach (var row in rows)
                {
                    var cells = row.Descendants<Cell>();
                    string id = null, parentId = null, text = null;
                    foreach (var cell in cells)
                    {
                        var cellReference = cell.CellReference;
                        var columnName = GetColumnName(cellReference);
                        if (columnName == ColumnMapping.Id)
                            id = cell.InnerText;
                        else if (columnName == ColumnMapping.ParentId)
                            parentId = cell.InnerText;
                        else if (columnName == ColumnMapping.Value)
                            text = cell.InnerText;
                    }
                    if (string.IsNullOrEmpty(id) && string.IsNullOrEmpty(parentId) && string.IsNullOrEmpty(text))
                        break;
                    items.Add(new Item
                    {
                        Id = string.IsNullOrEmpty(id) ? "" : id,
                        ParentId = string.IsNullOrEmpty(parentId) ? "" : parentId,
                        Text = text
                    });
                }

                return items;
            }
        }

        /// <summary>
        /// Привязка данных к именам столбцов
        /// </summary>
        private static class ColumnMapping
        {
            /// <summary>Наименование столбца идентификатора элемента</summary>
            public const string Id = "A";
            /// <summary>Наименование столбца идентификатора родительского элемента</summary>
            public const string ParentId = "B";
            /// <summary>Наименование столбца данных элемента</summary>
            public const string Value = "C";
        }

        /// <summary>
        /// Получение имени столбца из наименования ячейки
        /// </summary>
        /// <param name="cellReference">Наименование ячейки</param>
        /// <returns>Имя столбца</returns>
        private static string GetColumnName(string cellReference)
        {
            var regex = new Regex("[A-Za-z]+");
            var match = regex.Match(cellReference);

            return match.Value;
        }
    }
}