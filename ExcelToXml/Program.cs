﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ExcelToXml
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Incorrect usage!");
                Console.WriteLine("Synopsis: ExcelToXml <input_xlsx_file> <output_xml_file>");
            }
            var xlsxFilePath = args[0];
            var xmlFilePath = args[1];

            IEnumerable<Item> items;
            using (var fs = File.Open(xlsxFilePath, FileMode.Open, FileAccess.Read))
            {
                items = ExcelReader.Read(fs);
            }

            var itemsLinks = ForestBuilder.Build(items);

            using (var fs = File.Open(xmlFilePath, FileMode.Create, FileAccess.Write))
            {
                XmlWriter.Write(fs, itemsLinks);
            }
        }
    }
}