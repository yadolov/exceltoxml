﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ExcelToXml
{
    /// <summary>
    /// Контейнер для леса деревьев
    /// </summary>
    [XmlRoot(ElementName = "root")]
    public class ItemsRoot
    {
        /// <summary>
        /// Лес деревьев
        /// </summary>
        [XmlElement("item", Type = typeof(ItemLink))]
        public List<ItemLink> Items { get; set; }
    }
}