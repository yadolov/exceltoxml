﻿using QuickGraph;
using QuickGraph.Graphviz;
using System;
using System.Collections.Generic;
using System.IO;

namespace ExcelGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Incorrect usage!");
                Console.WriteLine("Synopsis: ExcelGenerator <output_xlsx_file>");
            }
            var xlsxFilePath = args[0];

            using (var fs = File.Create(xlsxFilePath))
            {
                var graph = BarabasiAlbertGraphGenerator.Generate(100);

                //using (var outfile = new StreamWriter("graph.dot"))
                //{
                //    outfile.Write(graph.ToGraphviz());
                //}

                ExcelGraphWriter.Write(fs, graph);
            }
        }
    }
}