﻿using QuickGraph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExcelGenerator
{
    internal static class BarabasiAlbertGraphGenerator
    {
        public static BidirectionalGraph<KeyValuePair<int, Guid>, Edge<KeyValuePair<int, Guid>>> Generate(int vertexCount)
        {
            var vertexId = 1;
            var graph = new BidirectionalGraph<KeyValuePair<int, Guid>, Edge<KeyValuePair<int, Guid>>>(true);
            graph.AddVerticesAndEdge(
                new Edge<KeyValuePair<int, Guid>>(
                    new KeyValuePair<int, Guid>(vertexId++, Guid.NewGuid()),
                    new KeyValuePair<int, Guid>(vertexId++, Guid.NewGuid())));

            var totalDegree = graph.Vertices.Aggregate(0, (acc, v) => acc + graph.Degree(v));
            var rnd = new Random();

            for (var ndx = vertexId; vertexId <= vertexCount; ++vertexId)
            {
                var vertex = new KeyValuePair<int, Guid>(vertexId, Guid.NewGuid());
                totalDegree = AddVertex(vertex, graph, totalDegree, rnd);
            }

            return graph;
        }

        private static int AddVertex(KeyValuePair<int, Guid> vertex, BidirectionalGraph<KeyValuePair<int, Guid>, Edge<KeyValuePair<int, Guid>>> graph,
            int totalDegree, Random rnd)
        {
            var inVerticies = new List<KeyValuePair<int, Guid>>();
            foreach (var v in graph.Vertices)
            {
                double probability = (double)graph.Degree(v) / totalDegree;
                if (probability > rnd.NextDouble())
                    inVerticies.Add(v);
            }
            if (!inVerticies.Any())
                return totalDegree;

            graph.AddVertex(vertex);
            foreach (var v in inVerticies)
            {
                graph.AddEdge(new Edge<KeyValuePair<int, Guid>>(v, vertex));
            }

            return totalDegree + inVerticies.Count * 2;
        }
    }
}