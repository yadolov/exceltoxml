﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using QuickGraph;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ExcelGenerator
{
    internal static class ExcelGraphWriter
    {
        private const string SheetName = "Graph Sheet";

        public static void Write<TGraph>(Stream stream, TGraph graph)
            where TGraph : IBidirectionalIncidenceGraph<KeyValuePair<int, Guid>, Edge<KeyValuePair<int, Guid>>>, IVertexSet<KeyValuePair<int, Guid>>
        {
            if (stream == null)
                throw new ArgumentNullException("stream");
            if (graph == null)
                throw new ArgumentNullException("graph");

            using (var document = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook))
            {
                var workbookpart = document.AddWorkbookPart();
                workbookpart.Workbook = new Workbook();
                var worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                var sheetData = new SheetData();
                worksheetPart.Worksheet = new Worksheet(sheetData);
                var sheets = document.WorkbookPart.Workbook.
                    AppendChild<Sheets>(new Sheets());
                var sheet = new Sheet()
                {
                    Id = document.WorkbookPart
                        .GetIdOfPart(worksheetPart),
                    SheetId = 1,
                    Name = SheetName
                };
                sheets.AppendChild(sheet);

                UInt32 rowNdx = 1;
                var vertexSet = (IVertexSet<KeyValuePair<int, Guid>>)graph;
                var incidenceGraph = (IBidirectionalIncidenceGraph<KeyValuePair<int, Guid>, Edge<KeyValuePair<int, Guid>>>)graph;
                foreach (var vertex in vertexSet.Vertices)
                {
                    var inEdges = incidenceGraph.InEdges(vertex);
                    if (inEdges.Any())
                    {
                        foreach (var edge in inEdges)
                        {
                            var row = new Row { RowIndex = rowNdx };
                            sheetData.AppendChild(row);
                            var cellId = CreateTextCell(ColumnMapping.Id, rowNdx, vertex.Key.ToString());
                            row.AppendChild(cellId);
                            var cellParentId = CreateTextCell(ColumnMapping.ParentId, rowNdx, edge.Source.Key.ToString());
                            row.AppendChild(cellParentId);
                            var cellValue = CreateTextCell(ColumnMapping.Value, rowNdx, vertex.Value.ToString());
                            row.AppendChild(cellValue);
                        }
                    }
                    else
                    {
                        var row = new Row { RowIndex = rowNdx };
                        sheetData.AppendChild(row);
                        var cellId = CreateTextCell(ColumnMapping.Id, rowNdx, vertex.Key.ToString());
                        row.AppendChild(cellId);
                        var cellValue = CreateTextCell(ColumnMapping.Value, rowNdx, vertex.Value.ToString());
                        row.AppendChild(cellValue);
                    }

                    ++rowNdx;
                }

                workbookpart.Workbook.Save();
                document.Close();
            }
        }

        private static class ColumnMapping
        {
            public const string Id = "A";
            public const string ParentId = "B";
            public const string Value = "C";
        }

        private static Cell CreateTextCell(string header, UInt32 index, string text)
        {
            var cell = new Cell
            {
                DataType = CellValues.InlineString,
                CellReference = header + index
            };

            var istring = new InlineString();
            var t = new Text { Text = text };
            istring.AppendChild(t);
            cell.AppendChild(istring);
            return cell;
        }
    }
}